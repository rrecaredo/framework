﻿using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Visigoth.Framework.Utils
{
    public static class DynamicCompiler
    {
        public static object Eval(string code, Type outType = null, string[] includeNamespaces = null, string[] includeAssemblies = null)
        {
            StringBuilder namespaces = null;
            object methodResult = null;

            using (CSharpCodeProvider codeProvider = new CSharpCodeProvider())
            {
                CompilerParameters compileParams = new CompilerParameters();

                compileParams.CompilerOptions = "/t:library";
                compileParams.GenerateInMemory = true;

                if (includeAssemblies != null && includeAssemblies.Any())
                {
                    foreach (string _assembly in includeAssemblies)
                    {
                        compileParams.ReferencedAssemblies.Add(_assembly);
                    }
                }

                if (includeNamespaces != null && includeNamespaces.Any())
                {
                    foreach (string _namespace in includeNamespaces)
                    {
                        namespaces = new StringBuilder();
                        namespaces.Append(string.Format("using {0};\n", _namespace));
                    }
                }
                code = string.Format(
                    @"{1}
            using System;
            namespace CSharpCode{{
                public class Parser{{
                    public {2} Eval(){{
                        {3} {0};
                    }}
                }}
            }}",
                    code,
                    namespaces != null ? namespaces.ToString() : null,
                    outType != null ? outType.FullName : "void",
                    outType != null ? "return" : string.Empty
                    );
                CompilerResults compileResult = codeProvider.CompileAssemblyFromSource(compileParams, code);

                if (compileResult.Errors.Count > 0)
                {
                    throw new Exception(compileResult.Errors[0].ErrorText);
                }
                Assembly assembly = compileResult.CompiledAssembly;
                object classInstance = assembly.CreateInstance("CSharpCode.Parser");
                Type type = classInstance.GetType();
                MethodInfo methodInfo = type.GetMethod("Eval");

                methodResult = methodInfo.Invoke(classInstance, null);
            }
            return methodResult;
        }
    }
}
