﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Visigoth.Framework.Utils
{
    public static class Hashing
    {
        public static string md5encrypt(string phrase)
        {
            UTF8Encoding encoder = new UTF8Encoding();
            MD5CryptoServiceProvider md5hasher = new MD5CryptoServiceProvider();
            byte[] hashedDataBytes = md5hasher.ComputeHash(encoder.GetBytes(phrase));

            return byteArrayToString(hashedDataBytes);
        }

        public static string sha1encrypt(string phrase)
        {
            UTF8Encoding encoder = new UTF8Encoding();
            SHA1CryptoServiceProvider sha1hasher = new SHA1CryptoServiceProvider();
            byte[] hashedDataBytes = sha1hasher.ComputeHash(encoder.GetBytes(phrase));
            return byteArrayToString(hashedDataBytes);
        }

        public static string sha256encrypt(string phrase)
        {
            UTF8Encoding encoder = new UTF8Encoding();
            SHA256Managed sha256hasher = new SHA256Managed();
            byte[] hashedDataBytes = sha256hasher.ComputeHash(encoder.GetBytes(phrase));

            return byteArrayToString(hashedDataBytes);
        }

        public static string sha384encrypt(string phrase)
        {
            UTF8Encoding encoder = new UTF8Encoding();
            SHA384Managed sha384hasher = new SHA384Managed();
            byte[] hashedDataBytes = sha384hasher.ComputeHash(encoder.GetBytes(phrase));

            return byteArrayToString(hashedDataBytes);
        }

        public static string sha512encrypt(string phrase)
        {
            UTF8Encoding encoder = new UTF8Encoding();
            SHA512Managed sha512hasher = new SHA512Managed();
            byte[] hashedDataBytes = sha512hasher.ComputeHash(encoder.GetBytes(phrase));

            return byteArrayToString(hashedDataBytes);
        }

        public static string byteArrayToString(byte[] inputArray)
        {
            string hex = string.Empty;

            foreach (byte x in inputArray)
            {
                hex += String.Format("{0:x2}", x);
            }
            return hex;
        }

        /// <summary>
        /// Generates a random, but readable, password, at least 8 characters and containing one or more
        /// lowercase and uppercase letters, numbers, and symbols.
        /// </summary>
        /// <returns>The random password</returns>

        public static string GenerateReadablePassword(string word1)
        {
            word1 = word1.Replace(" ", "").ToLower();

            string[] words = new string[3] { word1, word1, word1 };
            string alphabets = "abcdefghjkmnpqrstuvwxyz";
            string numbers = "3456789";
            string symbols = "3456789";
            string[] formats = new string[18]
            {
            "{1}{2}{3}{0}",
            "{1}{2}{0}{3}", "{2}{3}{1}{0}", "{3}{0}{2}{1}",
            "{0}{2}{3}{1}", "{1}{3}{0}{2}", "{3}{1}{2}{0}",
            "{0}{2}{1}{3}", "{1}{3}{2}{0}", "{2}{0}{3}{1}", "{3}{1}{0}{2}",
            "{0}{3}{1}{2}", "{1}{0}{2}{3}", "{2}{1}{3}{0}",
            "{0}{3}{2}{1}", "{1}{0}{3}{2}", "{2}{1}{0}{3}", "{3}{2}{1}{0}"
            };

            string password = string.Format(
              GetRandomString(formats),
              GetRandomString(words),
              GetRandomStringCharacter(alphabets),
              GetRandomStringCharacter(numbers),
              GetRandomStringCharacter(symbols)
            );

            while (password.Length < 18)
            {
                password += GetRandomStringCharacter(numbers);
            }

            return password;
        }

        public static Random _randomInstance = new Random();

        /// <summary>
        /// Gets the random string character.
        /// </summary>
        /// <param name="inputString">The input string.</param>

        public static char GetRandomStringCharacter(string inputString)
        {
            if (inputString.Length != 0)
            {
                return inputString[_randomInstance.Next(0, inputString.Length)];
            }
            else
            {
                return new char();
            }
        }

        public static string GetRandomString(string[] inputStringArray)
        {
            return inputStringArray[_randomInstance.Next(0, inputStringArray.Length)];
        }
    }
}
