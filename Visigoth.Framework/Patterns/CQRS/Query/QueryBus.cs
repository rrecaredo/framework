﻿using SimpleInjector;

namespace Visigoth.Framework.CQRS
{
    public class QueryBus : IQueryBus
    {
        Container _container;

        public QueryBus(Container container)
        {
            _container = container;
        }

        public TResult Submit<TParameter, TResult>(TParameter query)
            where TParameter : IQuery
            where TResult : IQueryResult
        {
            var handler = _container.GetInstance<IQueryHandler<TParameter, TResult>>();
            return handler.Retrieve(query);
        }

    }
}
