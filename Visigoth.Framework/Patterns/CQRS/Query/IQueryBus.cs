﻿namespace Visigoth.Framework.CQRS
{
    public interface IQueryBus
    {
        TResult Submit<TParameter, TResult>(TParameter query)
            where TParameter : IQuery
            where TResult : IQueryResult;
    }
}
