﻿using System.Collections.Generic;

namespace Visigoth.Framework.CQRS
{
    public interface ICommandBus
    {
        ICommandResult Submit<TCommand>(TCommand command)
            where TCommand : ICommand;

        IEnumerable<ValidationResult> Validate<TCommand>(TCommand command)
            where TCommand : ICommand;
    }
}
