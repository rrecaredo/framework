﻿namespace Visigoth.Framework.CQRS
{
    public interface ICommandResult
    {
        bool Success { get; }
    }
}
