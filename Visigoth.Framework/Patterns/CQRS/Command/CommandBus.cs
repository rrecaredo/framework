﻿using SimpleInjector;
using System.Collections.Generic;

namespace Visigoth.Framework.CQRS
{
    public class CommandBus : ICommandBus
    {
        Container _container;

        public CommandBus(Container container)
        {
            _container = container;
        }

        public ICommandResult Submit<TCommand>(TCommand command) where TCommand : ICommand
        {
            var handler = _container.GetInstance<ICommandHandler<TCommand>>();

            if (!((handler != null) && handler is ICommandHandler<TCommand>))
            {
                throw new CommandHandlerNotFoundException(typeof(TCommand));
            }
            return handler.Execute(command);

        }
        public IEnumerable<ValidationResult> Validate<TCommand>(TCommand command) where TCommand : ICommand
        {
            var handler = _container.GetInstance<IValidationHandler<TCommand>>();

            if (!((handler != null) && handler is IValidationHandler<TCommand>))
            {
                throw new ValidationHandlerNotFoundException(typeof(TCommand));
            }
            return handler.Validate(command);
        }
    }
}
