﻿using System.Collections.Generic;

namespace Visigoth.Framework.CQRS
{
    public interface IValidationHandler<in TCommand> where TCommand : ICommand
    {
        IEnumerable<ValidationResult> Validate(TCommand command);
    }
}
