﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Visigoth.Framework.Patterns
{
    /// <summary>
    /// Singleton Factory for multiple Singleton Instances at the same time.
    /// </summary>
    class SingletonFactory<T>
    {
        public readonly Dictionary<Type, object> _instances = new Dictionary<Type, object>();

        static readonly object _padlock = new object();

        private SingletonFactory()
        { 
        }

        public object getInstance(Type objectType)
        {
            lock (_padlock)
            {
                if (_instances.ContainsKey(objectType))
                {
                    return _instances[objectType];
                }
                else
                {
                    throw new Exception();
                }
            }
        }

        void LoadTypesICanReturn()
        {
            Type[] typesInThisAssembly = Assembly.GetExecutingAssembly().GetTypes();

            foreach (Type type in typesInThisAssembly)
            {
                if (type.GetInterface(typeof(T).ToString()) != null)
                {
                    _instances.Add(type,Activator.CreateInstance(type));
                }
            }
        }
    }
}
