﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Visigoth.Framework.Patterns
{
    /// <summary>
    /// Singleton Wraper to avoid multiple instances creation for a specific type.
    /// </summary>
    /// <typeparam name="T">Encapsulated object</typeparam>
    public abstract class Singleton<T> where T : IDisposable, new()
    {
        protected Singleton()
        {
        }

        private static   T      _instance = default(T);   // Wrapped Instance
        static  readonly object _padlock  = new object(); // Lock Key

        public static T Instance
        {
            get
            {
                try
                {
                    if (_instance == null)
                    {
                        lock (_padlock)
                        {
                            if (_instance == null)
                            {
                                _instance = Activator.CreateInstance<T>();
                            }
                        }
                    }
                    return _instance;
                }
                catch(Exception e)
                {
                    throw;
                }
            }
        }
    }
}
